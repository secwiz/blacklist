#!/bin/bash

function load_blacklist {
  ipset create zone_blacklist hash:net 2>/dev/null
  ipset destroy zone_blacklist2 >& /dev/null # destroy is if needed
  ipset create zone_blacklist2 hash:net # (re)create it

  echo "Building new blacklist" | logger
  while read CIDR; do
    # add the ip to the newly created blacklistt
    ipset add zone_blacklist2 $CIDR
  done < /opt/blacklist/zone_blacklist.nets

  echo "Activating new blacklist" | logger
  ipset swap zone_blacklist zone_blacklist2
  ipset destroy zone_blacklist2
}
 
#echo Creating blacklist
load_blacklist

firewall-cmd --reload
