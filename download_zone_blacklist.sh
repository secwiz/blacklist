PATH=/usr/bin:/usr/sbin:/sbin:/bin
url=http://www.ipdeny.com/ipblocks/data/countries
zones="cn ru hk in bd pk id jp kp kr my ng ph sg tw th"
# cn	China
# ru	Russia
# hk	Hong Kong
# pk	Pakistan
# id	India
# jp	Japan
# kp	North Korea
# kr	South Kore
# my	Malaysia
# ng	Nigeria
# ph	Philippines
# sg	Singapore
# tw	Taiwan
# th	Thailand

rm -f /opt/blacklist/zone_blacklist.nets 2>/dev/null
for zone in $zones
do
  for CIDR in `wget -q $url/$zone.zone -O -|grep -v ^#`; do
    echo $CIDR >> /opt/blacklist/zone_blacklist.nets 
  done
done

