# cron
sudo ln -s /opt/blacklist/reload_zone_blacklist.sh /etc/cron.daily/

# firewalld
legg til i /etc/firealld/direct.xml:

<rule ipv="ipv4" table="filter" chain="INPUT" priority="0">-m set --set zone_blacklist src -j DROP</rule>

# service
Slik at vi kjører load på boot

chmod 744 load_zone_blacklist.sh
sudo ln -s /opt/blacklist/blacklist.service /lib/systemd/system/blacklist.service
sudo systemctl enable blacklist.service
