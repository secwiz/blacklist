# https://mattwilcox.net/web-development/unexpected-ddos-blocking-china-with-ipset-and-iptables
# http://www.bilbos-stekkie.com/network/ipset-block-for-china/

# make_zone_blacklist
# load the zone blacklist for an iptables based firewall
# Written by Peter Kaagman <prjv.kaagman@gmail.com>;
# version 1.0.0 20150718
 
# Set the path so you know which executable you run
# being paranoid?
PATH=/usr/bin:/usr/sbin:/sbin:/bin
# The base url for the list at ipdeny
url=http://www.ipdeny.com/ipblocks/data/countries
# The zones we are going to deny access
zones="cn ru hk in bd pk id jp kp kr my ng ph sg tw th"

# cn	China
# ru	Russia
# hk	Hong Kong
# pk	Pakistan
# id	India
# jp	Japan
# kp	North Korea
# kr	South Kore
# my	Malaysia
# ng	Nigeria
# ph	Philippines
# sg	Singapore
# tw	Taiwan
# th	Thailand

# make sure zone_blacklist exists
ipset create zone_blacklist hash:net 2>/dev/null
 
function load_blacklist {
  #
  # Load the blacklist in a copy list
  ipset destroy zone_blacklist2 >& /dev/null # destroy is if needed
  ipset create zone_blacklist2 hash:net # (re)create it

  echo "Building new zone blacklist" | logger
  #
  # Iterate the zones
  for zone in $zones
  do
    # Download the new list
    # and itterate it
    echo "Adding zone $zone to blacklist" | logger
    for CIDR in `wget -q $url/$zone.zone -O -|grep -v ^#`; do
      # add the ip to the newly created blacklist2
      ipset add zone_blacklist2 $CIDR
    done
  done
  #
  # Swap this list with the excisting one
  # The list blacklist should be created by you iptables script
  echo "Acticating new blacklist" | logger
  ipset swap zone_blacklist zone_blacklist2
  #
  # Remove zone_blacklist2
  ipset destroy zone_blacklist2
}
 
#echo Creating blacklist
load_blacklist

firewall-cmd --reload
